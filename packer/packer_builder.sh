#!/bin/bash
set -e

sudo apt-get -y update
sudo apt-get -y upgrade
sudo apt-add-repository ppa:nginx/stable -y
sudo apt-get install -y python nginx git python-pip build-essential
sudo pip install uwsgi
sudo systemctl enable nginx

# in case if want to download something from git, we could store deploy key for git in s3 download it
# and this clone something from git
# /usr/local/bin/aws --region ${REGION} s3 cp --recursive s3://${METADATA_BUCKET} ${METADATA_DIR}"
# cp ${METADATA_DIR}/deploy_key /home/ubuntu/.ssh/id_rsa

mkdir /srv/app
