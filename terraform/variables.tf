variable "test_vpc" {
  type = "map"
  default = {
    name = "test_vpc"
    cidr = "100.80.0.0/20"
    availability_zones = "eu-central-1a"
    private_subnets = "100.80.0.0/24"
    public_subnets = "100.80.10.0/24"
  }
}
