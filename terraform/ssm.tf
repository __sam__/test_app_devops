resource "aws_ssm_parameter" "sns_topic_arn" {
  name  = "sns_topic_arn"
  type  = "String"
  value = "${aws_sns_topic.app_type1_send_email.arn}"
}
