terraform {
  backend "s3" {
    bucket = "test-terraform-state"
    key    = "terraform.tfstate"
    region = "eu-central-1"
  }
}

# define main aws provider
provider "aws" {
  version = "~> 1.33"
  region = "eu-central-1"
}
