data "template_file" "cloud_config_dev" {
  template = "${file("${path.module}/cloud-config.yml")}"

  vars {
    env = "dev"
  }
}

data "aws_ami" "application_type1_ubuntu" {
  most_recent = true
  filter {
    name = "name"
    values = ["app-type1-*"]
  }
  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
  filter {
    name = "architecture"
    values = ["x86_64"]
  }
  filter {
    name = "root-device-type"
    values = ["ebs"]
  }
  owners = ["self"]
}

resource "aws_launch_configuration" "app_dev" {
  name_prefix = "dev-"
  image_id = "${data.aws_ami.application_type1_ubuntu.id}"
  instance_type = "t2.micro"
  iam_instance_profile = "${aws_iam_instance_profile.freighter_infra.id}"
  key_name = "key-dev"
  security_groups = [
    "${aws_security_group.allow_all.id}",
  ]
  root_block_device = {
    volume_type = "gp2"
    volume_size = 40
    delete_on_termination = true
  }
  lifecycle {
    create_before_destroy = true
  }
  user_data = "${data.template_file.cloud_config_dev.rendered}"
}

resource "aws_autoscaling_group" "app_dev" {
  name = "dev-app"
  max_size = 3
  min_size = 3
  desired_capacity = 3
  default_cooldown = 60
  health_check_grace_period = 300
  health_check_type = "EC2"
  launch_configuration = "${aws_launch_configuration.app_dev.name}"
  vpc_zone_identifier = ["${aws_subnet.test_vpc_private.*.id}"]
  termination_policies = ["OldestLaunchConfiguration"]
  protect_from_scale_in = false
  wait_for_capacity_timeout = 0
  tag {
    key = "Name"
    value = "dev-app-type1"
    propagate_at_launch = true
  }
}

resource "aws_lb_target_group" "test_app" {
  name     = "test-app"
  port     = "5000"
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.test_vpc.id}"
  stickiness {
    type = "lb_cookie"
    cookie_duration = 86400 # 1 day
    enabled = true
  }

  health_check {
    path = "/status"
    protocol = "HTTP"
    port = "5000"
  }
  depends_on = ["aws_lb.test_app"]
}

resource "aws_lb_listener" "test_app" {
  load_balancer_arn = "${aws_lb.test_app.arn}"
  port              = 5000
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.test_app.arn}"
  }
  depends_on = ["aws_lb.test_app"]
}

resource "aws_lb" "test_app" {
  name               = "test-app"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.test_app.id}"]
  subnets            = ["${aws_subnet.test_vpc_public.*.id}"]

  enable_deletion_protection = false

  tags {
    env = "dev"
    app = "test_app"
  }
}

resource "aws_autoscaling_attachment" "test_app" {
  alb_target_group_arn   = "${aws_lb_target_group.test_app.arn}"
  autoscaling_group_name = "${aws_autoscaling_group.app_dev.id}"
}
