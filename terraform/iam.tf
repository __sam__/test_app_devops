data "aws_iam_policy_document" "instance_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "app_test" {
  statement {
    actions = [
      "s3:ListBucket",
    ]
    resources = [
      "${aws_s3_bucket.app_test.arn}"
    ]
  }

  statement {
    actions = [
      "s3:*",
    ]
    resources = [
      "${aws_s3_bucket.app_test.arn}/*"
    ]
  }

  statement {
    actions = [
      "SNS:Publish"
    ]
    resources = [
      "arn:aws:sns:*"
    ]
  }

  statement {
    actions = [
      "kms:Decrypt",
      "kms:DescribeKey",
      "kms:GetKeyPolicy",
      "kms:GetKeyRotationStatus",
      "kms:GetParametersForImport",
      "kms:ListAliases",
      "kms:ListGrants",
      "kms:ListKeyPolicies",
      "kms:ListKeys",
      "kms:ListResourceTags",
      "kms:ListRetirableGrants",
      "ssm:GetParameterHistory",
      "ssm:GetParameter",
      "ssm:GetParameters",
      "ssm:GetParametersByPath",
    ]
    ],
    resources = [
      "arn:aws:kms:*",
      "arn:aws:ssm:*"
    ]
  }
}

resource "aws_iam_policy" "app_type1" {
  name        = "app_type1"
  description = "Policy for application type1"
  policy = "${data.aws_iam_policy_document.app_test.json}"
}

resource "aws_iam_role" "app_type1" {
  name = "app_type1_role"
  assume_role_policy = "${data.aws_iam_policy_document.instance_assume_role.json}"
}

resource "aws_iam_instance_profile" "app_type1" {
  name = "app_type1"
  role = "${aws_iam_role.app_type1.name}"
}

resource "aws_iam_role_policy_attachment" "app_type1" {
  role       = "${aws_iam_role.app_type1.name}"
  policy_arn = "${aws_iam_policy.app_type1.arn}"
}
