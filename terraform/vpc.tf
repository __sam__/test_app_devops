#  vpc
resource "aws_vpc" "test_vpc" {
  cidr_block = "${var.test_vpc["cidr"]}"
  enable_dns_hostnames = true
  tags = {
    Name = "${var.test_vpc["name"]}"
  }
}

# test_vpc vpc private subnets
resource "aws_subnet" "test_vpc_private" {
  count = "${length(split(",", var.test_vpc["availability_zones"]))}"
  vpc_id = "${aws_vpc.test_vpc.id}"
  cidr_block = "${element(split(",",var.test_vpc["private_subnets"]), count.index)}"
  availability_zone = "${element(split(",", var.test_vpc["availability_zones"]), count.index)}"
  map_public_ip_on_launch = false
  tags {
    Name = "${var.test_vpc["name"]}_private_${replace(element(split(",", var.test_vpc["availability_zones"]), count.index),var.aws["region"],"")}"
  }
}

# test_vpc vpc public subnets
resource "aws_subnet" "test_vpc_public" {
  count = "${length(split(",", var.test_vpc["availability_zones"]))}"
  vpc_id = "${aws_vpc.test_vpc.id}"
  cidr_block = "${element(split(",",var.test_vpc["public_subnets"]), count.index)}"
  availability_zone = "${element(split(",", var.test_vpc["availability_zones"]), count.index)}"
  map_public_ip_on_launch = true
  tags {
    Name = "${var.test_vpc["name"]}_public_${replace(element(split(",", var.test_vpc["availability_zones"]), count.index),var.aws["region"],"")}"
  }
}

# test_vpc vpc internet gateway
resource "aws_internet_gateway" "test_vpc" {
  vpc_id = "${aws_vpc.test_vpc.id}"
  tags = {
    Name = "${var.test_vpc["name"]}_igw"
  }
}

# test_vpc vpc nat eip
# resource "aws_eip" "test_vpc_nat" {
#   vpc = true
# }

# test_vpc vpc nat gateway (single)
resource "aws_nat_gateway" "test_vpc" {
  allocation_id = "${aws_eip.test_vpc_nat.id}"
  subnet_id = "${element(aws_subnet.test_vpc_public.*.id, 0)}"
  depends_on = ["aws_internet_gateway.test_vpc","aws_eip.test_vpc_nat"]
}

# test_vpc vpc public route table (single)
resource "aws_route_table" "test_vpc_public" {
  vpc_id = "${aws_vpc.test_vpc.id}"
  tags {
    Name = "${var.test_vpc["name"]}_public"
  }
}

# test_vpc vpc public route table (single)
resource "aws_route_table" "test_vpc_private" {
  vpc_id = "${aws_vpc.test_vpc.id}"
  tags = {
    Name = "${var.test_vpc["name"]}_private"
  }
}

# test_vpc vpc route table assoc for private subnets
resource "aws_route_table_association" "test_vpc_private" {
  count = "${length(split(",", var.test_vpc["availability_zones"]))}"
  subnet_id = "${element(aws_subnet.test_vpc_private.*.id, count.index)}"
  route_table_id = "${aws_route_table.test_vpc_private.id}"
}

# test_vpc vpc route table assoc for public subnets
resource "aws_route_table_association" "test_vpc_public" {
  count = "${length(split(",", var.test_vpc["availability_zones"]))}"
  subnet_id = "${element(aws_subnet.test_vpc_public.*.id, count.index)}"
  route_table_id = "${aws_route_table.test_vpc_public.id}"
}

# test_vpc vpc default route for private subnets
resource "aws_route" "test_vpc_default_gw_private" {
  route_table_id = "${aws_route_table.test_vpc_private.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id = "${aws_nat_gateway.test_vpc.id}"
  depends_on = ["aws_nat_gateway.test_vpc","aws_route_table.test_vpc_private"]
}

# test_vpc vpc default route for public subnets
resource "aws_route" "test_vpc_default_gw_public" {
  route_table_id = "${aws_route_table.test_vpc_public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = "${aws_internet_gateway.test_vpc.id}"
  depends_on = ["aws_internet_gateway.test_vpc","aws_route_table.test_vpc_public"]
}

#################### outputs #######################

# output "test_vpc_nat_eip" {
#   value = "${aws_eip.test_vpc_nat.public_ip}"
# }
