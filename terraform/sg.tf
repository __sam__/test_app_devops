esource "aws_security_group" "test_app" {
  name        = "http"
  description = "Allow HTTP from anywhere"
  vpc_id      = "${aws_vpc.test_vpc.id}"
  revoke_rules_on_delete = "false"
}

resource "aws_security_group_rule" "http_80" {
  type        = "ingress"
  from_port   = 80
  to_port     = 80
  protocol    = "6" # TCP
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.test_app.id}"
}

resource "aws_security_group_rule" "https_443" {
  type        = "ingress"
  from_port   = 443
  to_port     = 443
  protocol    = "6" # TCP
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.test_app.id}"
}

resource "aws_security_group_rule" "http_3000" {
  type        = "ingress"
  from_port   = 5000
  to_port     = 5000
  protocol    = "6" # TCP
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.test_app.id}"
}
