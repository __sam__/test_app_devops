Overview
---


Workflow:
Jenkins poling changes from the repository(alternatively might be proceed with gitserver webhooks). If they were detected run build step. It clone git repository and run packer. 
Packer launch ec2 instance, install dependencies, clone there current version of application code. Build and publish AMI.

Deploying the new version of code is proceeding with applying terraform wich detecting the new version of ami and apply it for ASG.

Launch configuration for ASG use cloud_config.yml where we put configuration files for applications and required env variables
